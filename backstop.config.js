const config = require('./backstop.json')

module.exports = (options = {}) => {
  const refHost = options.refHost || 'http://uadigital.arizona.edu/ua-bootstrap'
  const testHost = options.testHost || 'http://localhost:8888'

  config.scenarios = [
    {
      'label': 'UA Bootstrap Homepage',
      'referenceUrl': refHost + '/',
      'url': testHost + '/',
      'hideSelectors': [],
      'removeSelectors': [],
      'selectorExpansion': true,
      'selectors': [
        'body',
        '.bs-footer'
      ],
      'readyEvent': null,
      'delay': 500,
      'misMatchThreshold': 100.00
    },
    {
      'label': 'UA Bootstrap Javascript Page',
      'referenceUrl': refHost + '/javascript.html',
      'url': testHost + '/javascript.html',
      'hideSelectors': [],
      'removeSelectors': [],
      'selectorExpansion': true,
      'selectors': [
        'body'
      ],
      'readyEvent': null,
      'delay': 500,
      'misMatchThreshold': 100.00
    }
  ]

  return config
}
