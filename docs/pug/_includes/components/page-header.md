<h1 id="page-header">Page header</h1>

<p>A simple shell for an <code>h1</code> to appropriately space out and segment sections of content on a page. It can utilize the <code>h1</code>'s default <code>small</code> element, as well as most other components (with additional styles).</p>
<div class="example" data-example-id="simple-page-header">
	<p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
	<div class="page-header">
	  <h1>Example page header <small>Subtext for header</small></h1>
	</div>
</div>

```html
<div class="page-header">
<h1>Example page header <small>Subtext for header</small></h1>
</div>
```