
### Image overlays

Turn an image into a card background and overlay your card's text. Depending on the image, you may or may not need `.card-inverse` (see below).

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="card card-inverse">
  <img class="card-img" data-src="holder.js/100px270?bg=333333&fg=49595E&text=Card image" alt="Card image">
  <div class="card-img-overlay">
    <h4 class="card-title">Card title</h4>
    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
  </div>
</div>
</div>

```html
<div class="card card-inverse">
  <img class="card-img" data-src="holder.js/100px270?bg=333333&fg=49595E&text=Card image" alt="Card image">
  <div class="card-img-overlay">
    <h4 class="card-title">Card title</h4>
    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
  </div>
</div>
```

### Image with caption

Add a caption that is nested within your image.

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="card card-inverse">
    <a href="#">
      <img class="card-img" src="https://placeimg.com/320/270/animals" alt="Card image">
      <div class="card-img-caption">
        <div class="h4 card-title text-center">Card title</div>
      </div>
    </a>
  </div>
</div>

```html
<div class="card card-inverse">
  <a href="#">
    <img class="card-img" src="https://placeimg.com/320/270/animals" alt="Card image">
    <div class="card-img-caption">
      <div class="h4 card-title text-center">Card title</div>
    </div>
  </a>
</div>
```


