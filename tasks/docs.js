const $ = {
  merge: require('merge-stream'),
  pug: require('gulp-pug'),
  replace: require('gulp-replace')
}

const versionPlaceholder = '[VERSION]'
function getPackageVersion () {
  return `v${require('../package.json').version}`
}

module.exports = {
  /* Compiles the PUG Documentation into HTML */
  pug: (gulp, config) => () => {
    const stream = gulp.src(config.docsDir + '/pug/*.pug')
      .pipe($.pug({ pretty: true }))
      .pipe($.replace(versionPlaceholder, getPackageVersion()))
      .pipe(gulp.dest(config.buildDir))

    stream.on('end', config.bs.reload)
    return stream
  },

  /* Copies Node & Static assets to build folder */
  assets: (gulp, config) => () => {
    const highlightjs = gulp.src('node_modules/highlightjs/highlight.pack.min.js')
      .pipe(gulp.dest(`${config.buildDir}/js`))
    const holderjs = gulp.src('node_modules/holderjs/holder.min.js')
      .pipe(gulp.dest(`${config.buildDir}/js`))
    const gc = gulp.src('node_modules/highlightjs/styles/googlecode.css')
      .pipe(gulp.dest(`${config.buildDir}/css`))

    const html = gulp.src(`${config.docsDir}/examples/**`)
      .pipe($.replace(versionPlaceholder, getPackageVersion()))
      .pipe(gulp.dest(`${config.buildDir}/examples/`))

    const statics = gulp.src([
      `${config.docsDir}/**`,
      `!${config.docsDir}/examples/**`,
      `!${config.docsDir}/pug/**`,
      `!${config.docsDir}/pug`
    ]).pipe(gulp.dest(config.buildDir))

    const stream = $.merge(highlightjs, holderjs, gc, html, statics)
    stream.on('end', config.bs.reload)
    return stream
  }
}
