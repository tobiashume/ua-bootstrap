const $ = {
  del: require('del')
}

module.exports = {
  build: (gulp, config) => () => { return $.del.sync(['build/*']) },
  dist: (gulp, config) => () => { return $.del.sync(['dist/*', '!dist/home', '!dist/home/**']) }
}
