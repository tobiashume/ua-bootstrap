const $ = {
  concat: require('gulp-concat'),
  rename: require('gulp-rename'),
  uglify: require('gulp-uglify')
}

module.exports = {
  /* Minifies built JS file(s) */
  minify: (gulp, config) => () => {
    return gulp.src(config.buildDir + '/lib/ua-bootstrap.js')
      .pipe($.rename({ suffix: '.min' }))
      .pipe($.uglify())
      .pipe(gulp.dest(config.buildDir + '/lib'))
  },

  /* Concatinates all JS files into one distributable file */
  js: (gulp, config) => () => {
    const bootstrapJs = config.bootstrapDir + '/javascripts/bootstrap/'
    return gulp.src([
      bootstrapJs + 'transition.js',
      bootstrapJs + 'alert.js',
      bootstrapJs + 'button.js',
      bootstrapJs + 'carousel.js',
      bootstrapJs + 'collapse.js',
      bootstrapJs + 'dropdown.js',
      bootstrapJs + 'modal.js',
      bootstrapJs + 'tab.js',
      bootstrapJs + 'affix.js',
      bootstrapJs + 'scrollspy.js',
      bootstrapJs + 'tooltip.js',
      bootstrapJs + 'popover.js',
      config.srcDir + '/js/*.js'
    ])
      .pipe($.concat('ua-bootstrap.js'))
      .pipe(gulp.dest(config.buildDir + '/lib'))
      .pipe(config.bs.stream())
  }
}
